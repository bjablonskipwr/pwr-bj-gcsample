﻿using System;

namespace GCSample
{

    /// <summary>
    /// Circular reference sample class
    /// </summary>
    public class A
    {
        private B _b;
        private int[] _data = new int[1000000];
        public A(B b) { _b = b; }
    }

    public class B
    {
        A _a;
        public B() { _a = new A(this); }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ready to allocate");
            Console.ReadKey();

            B[] b = new B[100];
            for (int i = 0; i < b.Length; i++)
                b[i] = new B();

            Console.WriteLine($"Allocated: {GC.GetTotalMemory(false)}. Ready to collect.");
            Console.ReadKey();

            b = null;

            Console.WriteLine($"Allocated: {GC.GetTotalMemory(false)}. Reference removed");
            Console.ReadKey();

            GC.Collect();

            Console.WriteLine($"Allocated: {GC.GetTotalMemory(true)}. Forced collected");
            Console.ReadKey();

        }
    }
}
